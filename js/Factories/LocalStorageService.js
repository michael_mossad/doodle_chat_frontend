angular.module("DoodleChatApp").factory('localStorageService', function ($rootScope) {
    return {
        getData: function(key)
        {
            return JSON.parse(localStorage.getItem(key));
        },
        setData: function(key, data)
        {
            localStorage.setItem(key, JSON.stringify(data));
        },
        removeData: function(key)
        {
            localStorage.removeItem(key);
        },
        clearAll: function()
        {
            localStorage.clear();
        }
    };
});