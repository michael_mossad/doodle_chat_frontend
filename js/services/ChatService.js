angular.module('DoodleChatApp').service('ChatService', ['$http', '$q', '$rootScope',
    function ($http, $q, serverip, HEADERS, localStorageService, $rootScope) {
        this.getMesseges = function () {
            return $http.get("http://localhost:8082/DoodleChat/rest/messeges/get_messeges");
        };
        this.addMessege = function (messege) {
            messege = JSON.stringify(messege);
            return $http.post("http://localhost:8082/DoodleChat/rest/messeges/add_messeges", messege);
        };
    }
]);