/* Setup blank page controller */
angular.module('DoodleChatApp').controller('LogonController', ['$rootScope', '$scope', 'settings', 'localStorageService', '$state', function ($rootScope, $scope, settings, localStorageService, $state) {
    $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();

        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
    });
    $scope.logon = function () {
        localStorageService.setData("chatName", $scope.chatName);
        $state.go('chat');
    }
}]);