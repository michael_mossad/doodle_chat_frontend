/* Setup blank page controller */
angular.module('DoodleChatApp').controller('ChatController', ['$rootScope', '$scope', 'settings', 'ChatService', 'localStorageService', '$state', function ($rootScope, $scope, settings, ChatService, localStorageService, $state) {
    $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();
        // set default layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;

        $scope.owner = localStorageService.getData("chatName");
        if (!$scope.owner)
            $state.go("logon");
        $scope.getListMesseges();
        
        // refresh chat room
        setInterval(function(){ 
            $scope.getListMesseges();
         }, 3000);
    });
    $scope.getListMesseges = function () {
        $scope.promise = ChatService.getMesseges();
        $scope.promise.then(function (response) {
            App.stopPageLoading();
            $scope.messgesList = response.data;
            //scroll down
            var bottomCoord = angular.element(document.querySelector('#messagelist'))[0].scrollHeight;
            angular.element(document.querySelector('#messagelist')).slimScroll({
                scrollTo: bottomCoord + 5
            });
        }, function (error) {
            App.stopPageLoading();

            if (!angular.isObject(error.data)) {
                // $scope.error = [];
                // $scope.error.push(error.data);
                // $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            } else {
                // $scope.error = error.data;
                // $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    }
    $scope.addMessege = function () {
        if (!$scope.messege || !$scope.messege.messegeText || $scope.messege.messegeText.replace(/[\s]/g, '') == "")
            return;
        $scope.messege.createdOn = new Date();
        $scope.messege.owner = $scope.owner;
        $scope.promise = ChatService.addMessege($scope.messege);
        $scope.promise.then(function (response) {
            App.stopPageLoading();
            $scope.getListMesseges();
            $scope.messege = {};
        }, function (error) {
            App.stopPageLoading();
            if (!angular.isObject(error.data)) {
                // $scope.error = [];
                // $scope.error.push(error.data);
                // $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            } else {
                // $scope.error = error.data;
                // $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    }

}]);